let gulp = require('gulp');
let browserSync = require('browser-sync').create();
let reload = browserSync.reload;


gulp.task('default', function(){
    console.log('Gulp rodando');    
});

gulp.task('browser-sync', function(){
    browserSync.init({
        server: {
            baseDir: "./"
        }
    });

    gulp.watch("*.html").on("change", reload);

});