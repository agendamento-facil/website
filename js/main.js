/* ACCORDION HOME */

// Inicia a função quando o documento estiver pronto
$(document).ready(function () {
    // Pega todos os itens do accordion
    var accordionList = document.querySelectorAll('.card-header');
    // Duas variáveis, simples.
    var active = 'active';
    var show = 'show';

    // Se a lista de itens possuir itens, entra na função.
    if (accordionList.length) {
        // Quando o header de um item é clicado chama essa função.
        $('.card-header').click(function () {
            // Evita que o click faça a página ir para o topo, por causa do a[href] que tem no header.
            event.preventDefault();
            // Pega o item que foi clicado e faz o toggle da classe "active", nesse caso não está sendo utilizado pra nada essa classe, mas futuramente pode ser usada pra estilizar o header do item que está aberto.
            this.classList.toggle(active);
            // Pega o próximo elemento que nesse caso é o corpo do item da FAQ e faz o toggle da classe "show".
            this.nextElementSibling.classList.toggle(show);
        });
    }
});

/* END ACCORDION HOME */

/* HEADER SCROLL */
// let $target = $('.anime'),
//     $animationClass = 'anime-start';

// let p = $('.header').top;

// function animeScroll(){
//     let documentTop = $(document).scrollTop();
// console.log(documentTop);

//     if (documentTop > 1) {
//         $(target).addClass(animationClass);
//     } else {
//         $(this).removeClass(animationClass);
//     }
// };

// animeScroll();


/* END HEADER SCROLL */

/* Menu Mobile */
let btn = document.querySelector('#t');
let x = document.querySelector('.x');
let list_menu = document.querySelector('.lista-menu');

    btn.addEventListener('click', function(e){
        e.preventDefault();
        list_menu.classList.toggle('show');
        btn.classList.add('btn-teste');
        x.classList.toggle('btn-close');  
    });

    x.addEventListener('click', function(e){
        e.preventDefault();

        list_menu.classList.remove('show');
        btn.classList.remove('btn-teste');
        x.classList.remove('btn-close');        
    });



/* End Menu Mobile */


function ativo() {
    let modal = document.getElementById('modalHome');

    modal.classList.add('modal_ativo');
}

/* Modal */

let login = document.getElementById('login');

login.addEventListener('click', () => {
    let modal_login = document.querySelector('#modalHome');
    modal_login.classList.toggle('modal_ativo');

    let close = modal_login.querySelector('.close');
    close.addEventListener('click', (event) => {    
        event.preventDefault();
        modal_login.classList.remove('modal_ativo');
    });
});

/* End Modal */